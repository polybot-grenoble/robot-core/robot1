/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <malloc.h>
#include <math.h>
#include <stdbool.h>

#include "../../PolybotSrc/Gobelet/servo.h"
#include "../../PolybotSrc/Raspberry/lib_com_uart.h"
#include "../../PolybotSrc/sensors/ToF_sensors.h"
#include "../../PolybotSrc/Movement/action.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
bool action = false;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// redirect printf to uart2
void __io_putchar(uint8_t ch) {
  HAL_UART_Transmit(&huart2, &ch, 1, 1);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t rx_buff[RECEIVE_SIZE] = { 0 };

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  MX_RTC_Init();
  MX_TIM3_Init();
  MX_I2C2_Init();
  MX_TIM1_Init();
  MX_USART6_UART_Init();
  /* USER CODE BEGIN 2 */

  /* Enable UART6 interrupt */
  HAL_UART_Receive_IT(&huart6, rx_buff, RECEIVE_SIZE);

  Move_geometry g = {
    .whDist = 262, //larger between encoder wheels in mm
    .dmWh1 = 88, //encoder 1 wheel diameter in mm
    .dmWh2 = 88, //encoder 2 wheel diameter in mm
    .ticks1 = 4096, // number of ticks per revolution (encoder 1)
    .ticks2 = 4096, // number of ticks per revolution (encoder 2)
  };

  Move_settings settings = { .acceleration = 8000, .deceleration = 8000, .speed = 10000, .precision = 0.05, };

  Move m = move_init(&huart1, &g, &settings);
  HAL_StatusTypeDef s;

  // Initialization ToF sensors
  //HAL_Delay(100);
  //sensors_init(&hi2c2);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  printf("Ready\r\n");
  while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if (action == false) {
      continue;
    }
    action = false;

    // read data from uart
    HAL_UART_Receive_IT(&huart6, rx_buff, RECEIVE_SIZE);

    Frame f = decode_frame(rx_buff);
    printf("\r\nreceived frame: status=%d, order=%d, data=%ld, crc=%d\r\n", f.order, f.instruction, f.data, f.checksum);

    if (f.checksum != checksum(rx_buff, RECEIVE_SIZE)) {
      send_answer(&huart6, CHKERR, 0);
      continue;
    }

    if (f.order != ORDER_COMMAND) {
      send_answer(&huart6, QUERY_ERROR, 0);
      continue;
    }

    switch (f.instruction) {
    case CONNECTION_REQUEST: {
      printf("instruction: connection request \r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case LAUNCH_ROBOT: {
      printf("instruction: start button state \r\n");
      uint32_t state_launch_btn = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3);
      send_answer(&huart6, OK, state_launch_btn);
      break;
    }

    case DISTANCE: {
      int32_t distance = f.data;
      printf("instruction: distance of %ld mm\r\n", distance);
      s = move_straight(&m, distance);

      if (s == HAL_OK) {
        send_answer(&huart6, OK, f.data);
      } else {
        send_answer(&huart6, INTERNAL_ERROR, s);
      }
      break;
    }

    case ROTATION: {
      printf("instruction: rotation of %ld °\r\n", f.data);
      s = move_rotation(&m, f.data);
      if (s == HAL_OK) {
        send_answer(&huart6, OK, f.data);
      } else {
        send_answer(&huart6, INTERNAL_ERROR, s);
      }
      break;
    }

    case MOVE_STATE: {
      printf("instruction: movement state\r\n");
      if (is_move_finished(&m)) {
        printf("finished\r\n");
        send_answer(&huart6, OK, 0x0F);
      } else {
          printf("not finished\r\n");
        send_answer(&huart6, OK, 0x00);
      }
      break;
    }

    case MOVE_VALUE: {
      printf("instruction: movement value\r\n");
      if (f.data == 0x0) {
        int32_t d = get_current_distance(&m);
        send_answer(&huart6, OK, d);
        break;
      } else if (f.data == 0x1) {
        int32_t r = get_current_rotation(&m);
        send_answer(&huart6, OK, r);
        break;
      } else {
        send_answer(&huart6, UNKNOWN, f.data);
      }
      break;
    }

    case MOVE_STOP: {
      printf("instruction: stop robot\r\n");
      s = move_stop(&m);
      if (s == HAL_OK) {
        send_answer(&huart6, OK, 0);
      } else {
        send_answer(&huart6, INTERNAL_ERROR, s);
      }
      break;
    }

    case OBSTACLE_TOF: {
      printf("instruction: obstacle detected\r\n");
      uint16_t range = 170; // in [mm]
      uint8_t detected = sensors_measurement(&hi2c2, range);
      send_answer(&huart6, OK, detected);
      break;
    }

    case MOVE_RESTART: {
      printf("instruction: restart robot\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case ALLUMER_POMPE: {
      printf("instruction: allumer pompe\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case ETEINDRE_POMPE: {
      printf("instruction: eteindre pompe\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case TRANSPORT: {
      printf("instruction: transport\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case GALERIE_HAUT: {
      printf("instruction: galerie haut\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case GALERIE_BAS: {
      printf("instruction: galerie bas\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case SOL: {
      printf("instruction: sol\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case COTE_INCLINE: {
      printf("instruction: cote incline\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case COTE_HORIZONTAL: {
      printf("instruction: cote horizontal\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case REPLIE: {
      printf("instruction: replie\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    case DEPLOIE: {
      printf("instruction: deploie\r\n");
      printf("FIXME\r\n");
      send_answer(&huart6, OK, 0);
      break;
    }

    default: {
      send_answer(&huart6, UNKNOWN, 0);
      break;
    }
    }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
  if (huart->Instance == USART6) {
    action = true;
  }
}

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM10 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM10) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
